namespace kgcorlib
{
	/// <summary>
	/// Contains arithmetic Pose Mod commands that work on any application.
	/// </summary>
	public class ArithmeticCommands
	{
		/// <summary>
		/// Sets the variable named "<paramref name="variableName"/>" to `{variableName} + {value}`.
		/// </summary>
		/// <example>
		/// <code>
		/// [MyVariable]:10
		/// Add:MyVariable:8
		/// // `MyVariable` now has a value of `18`.
		/// </code>
		/// </example>
		public void Add(string variableName, float value) { }

		/// <summary>
		/// Sets the variable named "<paramref name="variableName"/>" to `{variableName} - {value}`.
		/// </summary>
		/// <example>
		/// <code>
		/// [MyVariable]:10
		/// Sub:MyVariable:15
		/// // `MyVariable` now has a value of `-5`.
		/// </code>
		/// </example>
		public void Sub(string variableName, float value) { }

		/// <summary>
		/// Sets the variable named "<paramref name="variableName"/>" to `{variableName} * {value}`.
		/// </summary>
		/// <example>
		/// <code>
		/// [MyVariable]:150
		/// Mul:MyVariable:6
		/// // `MyVariable` now has a value of `900`.
		/// </code>
		/// </example>
		public void Mul(string variableName, float value) { }

		/// <summary>
		/// Sets the variable named "<paramref name="variableName"/>" to `{variableName} / {value}`.
		/// </summary>
		/// <example>
		/// <code>
		/// [MyVariable]:100
		/// Div:MyVariable:3
		/// // `MyVariable` now has a value of `33.333...`.
		/// </code>
		/// </example>
		public void Div(string variableName, float value) { }
	}
}
