namespace kgcorlib
{
    public class GeneralCommands
    {
        /// <summary>
        /// A test command that writes "oof" a file named "`ran.txt`" in the application's root folder. If the file exists, it is overwritten.
        /// </summary>
        public void _test() { }
    }
}
