namespace kgcorlib
{
    /// <summary>
    /// Contains controw flow Pose Mod commands that work on any application.
    /// </summary>
    public class ControlFlowCommands
    {
        /// <summary>
        /// Ends the cutscene.
        /// </summary>
        public void End() { }

        /// <summary>
        /// Executes the `then` branch if <paramref name="condition"/> is true; executes the `else` branch if <paramref name="condition"/> is false.
        /// </summary>
        /// <remarks>
        /// You can use the `AND` and `OR` modifiers in the condition. If you use them, all conditions must be parenthesized.
        /// 
        /// Conditions are evaluated from left to right with no grouping. Therefore, `({A})AND({B})OR({C})` will be processed as:
        /// - `({A} and {B}) or ({C})`: A and B are both true, or C is true; not
        /// - `({A}) and ({B} or {C})`: A is true, and either B and C are true.
        /// Pay attention to the commas.
        /// </remarks>
        /// <example>
        /// <code>
        /// [MyVariable]:10
        /// 
        /// // If `MyVariable` is higher than or equal to `0` and not equal to `10`, or is equal to `-1`...
        /// if:(Larger:MyVariable:0)AND(NotEqual:MyVariable:10)OR(Equal:MyVariable:-1)
        /// then:Jmp:Label1 // If true, jump to `Label1`.
        /// else:End // If false, end the cutscene.
        /// 
        /// // [...]
        /// </code>
        /// </example>
        /// <param name="condition"></param>
        public void If(bool condition) { }
        
        /// <summary>
        /// Jumps to the label named "<paramref name="labelName"/>".
        /// </summary>
        /// <example>
        /// <code>
        /// Jmp:MyLabel
        /// [MyVariable]:0 // Skipped.
        /// Add:MyVariable:10 // Skipped.
        /// Label:MyLabel
        /// // `MyVariable` is undefined.
        /// </code>
        /// </example>
        public void Jmp(string labelName) { }
        
        /// <summary>
        /// Defines a label named "<paramref name="labelName"/>".
        /// </summary>
        /// <example>
        /// <code>
        /// // Locks the player's position to Vector3(0, 0, 0).
        /// Label:InfiniteLoop
        /// RefPosition:Player:0, 0, 0
        /// Jmp:InfiniteLoop
        /// </code>
        /// </example>
        public void Label(string labelName) { }

        /// <summary>
        /// Alias for [Label:{labelName}](xref:kgcorlib.ControlFlowCommands#kgcorlib_ControlFlowCommands_Label_System_String_).
        /// </summary>
        public void Lbl(string labelName) { }

        /// <summary>
        /// Skips the next command.
        /// </summary>
        /// <example>
        /// <code>
        /// [MyVariable]:0
        /// Skip
        /// Add:MyVariable:1 // This command will be skipped.
        /// // `MyVariable` now has a value of `0`.
        /// </code>
        /// </example>
        public void Skip() { }
    }
}
