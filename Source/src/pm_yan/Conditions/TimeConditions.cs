﻿namespace pm_yan
{
    /// <summary>
    /// Contains time-related Pose Mod conditions that only work on Yandere Simulator.
    /// </summary>
    public class TimeConditions
    {
        /// <summary>
        /// Checks if the current time is higher than or equal to <paramref name="minTime"/> and lower than or equal to <paramref name="maxTime"/>.
        /// </summary>
        /// <remarks>
        /// <paramref name="minTime"/> and <paramref name="maxTime"/> are both in hours since the start of the day.
        /// For example: `1` is `01:00`, `12` is `12:00`, `15.5` is `15:30` and `17.8` is `17:48`. `0.1` (`10%`) is equivalent to 6 minutes.
        /// </remarks>
        public bool Time(float minTime, float maxTime) => default;

        /// <summary>
        /// Checks if the current day of the week is <paramref name="value"/>.
        /// </summary>
        /// <remarks>
        /// <paramref name="value"/> may be in number or case-insensitive text form. For example: `2`, `tuesday`, `TUESDAY` and `tUeSdAy` all evaluate to the same day.
        /// </remarks>
        public bool Weekday(string value) => default;
    }
}
