﻿namespace pm_yan
{
    /// <summary>
    /// Contains Pose Mod conditions that only work on Yandere Simulator.
    /// </summary>
    public class GeneralConditions
    {
        /// <summary>
        /// Checks if the school's atmosphere is higher than or equal to <paramref name="value"/>.
        /// </summary>
        public bool Atmosphere(float value) => default;

        /// <summary>
        /// Checks if the prompt contained in <paramref name="prompt"/> is over 90% pressed.
        /// </summary>
        public bool PromptPressed(GameObject prompt) => default;
    }
}
