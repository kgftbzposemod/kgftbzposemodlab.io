﻿namespace pm_yan
{
    /// <summary>
    /// Contains player-related Pose Mod conditions that only work on Yandere Simulator.
    /// </summary>
    public class PlayerConditions
    {
        /// <summary>
        /// Checks if the player's sanity is higher than or equal to <paramref name="value"/>
        /// </summary>
        public bool Sanity(float value) => default;

        /// <summary>
        /// Checks if the player's grade in a skill is equal to <paramref name="value"/>.
        /// </summary>
        /// <remarks>
        /// If <paramref name="skill"/> is the name of the subject, the check is for the amount of study points invested in the subject, which ranges from 0 to 100.
        /// 
        /// If <paramref name="skill"/> is the name of the subject followed by `Grade`, the check is for the rank in the subject, ranging from 0 to 5.
        /// 
        /// The available subjects are: `Biology`, `Chemistry`, `Language`, `Physical` and `Psychology`.
        /// </remarks>
        public bool Skills(string skill, int value) => default;

        /// <summary>
        /// Checks if the player's grade in a skill is lower than <paramref name="value"/>.
        /// </summary>
        /// <remarks>
        /// If <paramref name="skill"/> is the name of the subject, the check is for the amount of study points invested in the subject, which ranges from 0 to 100.
        /// 
        /// If <paramref name="skill"/> is the name of the subject followed by `Grade`, the check is for the rank in the subject, ranging from 0 to 5.
        /// 
        /// The available subjects are: `Biology`, `Chemistry`, `Language`, `Physical` and `Psychology`.
        /// </remarks>
        public bool Skills2(string skill, int value) => default;

        /// <summary>
        /// Checks if the player is carrying the weapon with the ID <paramref name="id"/>.
        /// </summary>
        public bool Weapon(int id) => default;

        /// <summary>
        /// Checks if the player is carrying the 50kg weight.
        /// </summary>
        /// <remarks>
        /// <paramref name="value"/> is an unused parameter.
        /// </remarks>
        public bool Weight(int value) => default;

        /// <summary>
        /// Checks if the player is playing the idle animation.
        /// </summary>
        public bool YanIdle() => default;

        /// <summary>
        /// Checks if the distance between the player and the edit object is lower than <paramref name="distance"/>.
        /// </summary>
        /// <remarks>
        /// This is equivalent to [EditObjectNearOtherObject:YandereChan:{distance}](xref:kg_PoseMod.GameObjectConditions#kg_PoseMod_GameObjectConditions_EditObjectNearOtherObject_GameObject_System_Single_).
        /// </remarks>
        public bool YanNearEditObject(float distance) => default;

        /// <summary>
        /// Checks if the distance between the player and Vector3(<paramref name="x"/>, <paramref name="y"/>, <paramref name="z"/>) is lower than <paramref name="distance"/>.
        /// </summary>
        /// <remarks>
        /// This is equivalent to [YanNearPoint:{x}, {y}, {z}:{distance}](xref:pm_yan.PlayerConditions#pm_yan_PlayerConditions_YanNearPoint_Vector3_System_Single_).
        /// </remarks>
        public bool YanNearPoint(float x, float y, float z, float distance) => default;

        /// <summary>
        /// Checks if the distance between the player and <paramref name="position"/> is lower than <paramref name="distance"/>.
        /// </summary>
        /// <remarks>
        /// This is equivalent to [ObjectNearPoint:YandereChan:{location}:{distance}](xref:kg_PoseMod.GameObjectConditions#kg_PoseMod_GameObjectConditions_ObjectNearPoint_GameObject_Vector3_System_Single_)
        /// </remarks>
        public bool YanNearPoint(Vector3 location, float distance) => default;
    }
}
