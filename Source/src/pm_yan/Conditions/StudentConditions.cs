﻿namespace pm_yan
{
    /// <summary>
    /// Contains student-related Pose Mod conditions that only work on Yandere Simulator.
    /// </summary>
    public class StudentConditions
    {
        /// <summary>
        /// Checks if <paramref name="student"/> is non-existant, dead, or dying.
        /// </summary>
        public bool Dead(StudentScript student) => default;

        /// <summary>
        /// Checks if <paramref name="student"/>'s reputation is higher than or equal to <paramref name="value"/>.
        /// </summary>
        public bool StudReputationIsAbove(StudentScript student, float value) => default;

        /// <summary>
        /// Checks if <paramref name="student"/>'s reputation is lower than or equal to <paramref name="value"/>.
        /// </summary>
        public bool StudReputationIsUnder(StudentScript student, float value) => default;
    }
}
