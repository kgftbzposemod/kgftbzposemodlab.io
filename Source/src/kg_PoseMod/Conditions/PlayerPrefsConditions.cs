﻿namespace kg_PoseMod
{
	/// <summary>
	/// Contains PlayerPrefs-related Pose Mod conditions that work on any application.
	/// </summary>
	public class PlayerPrefsConditions
    {
		/// <summary>
		/// Checks if the value of the PlayerPref "<paramref name="name"/>" of type "int" is equal to <paramref name="value"/>.
		/// </summary>
		public bool PPInt(string name, int value) => default;

		/// <summary>
		/// Checks if the value of the PlayerPref "<paramref name="name"/>" of type "float" is equal to <paramref name="value"/>.
		/// </summary>
		public bool PPFloat(string name, float value) => default;

		/// <summary>
		/// Checks if the value of the PlayerPref "<paramref name="name"/>" of type "string" is equal to <paramref name="value"/>.
		/// </summary>
		public bool PPString(string name, string value) => default;

		/// <summary>
		/// Checks if the value of the PlayerPref "<paramref name="name"/>" of type "int" is different than <paramref name="value"/>.
		/// </summary>
		public bool PInt(string name, int value) => default;

		/// <summary>
		/// Checks if the value of the PlayerPref "<paramref name="name"/>" of type "float" is different than <paramref name="value"/>.
		/// </summary>
		public bool PFloat(string name, float value) => default;

		/// <summary>
		/// Checks if the value of the PlayerPref "<paramref name="name"/>" of type "string" is different than <paramref name="value"/>.
		/// </summary>
		public bool pstring(string name, string value) => default;
	}
}
