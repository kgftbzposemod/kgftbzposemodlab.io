﻿namespace kg_PoseMod
{
    /// <summary>
    /// Contains GameObject-related Pose Mod conditions that work on any application.
    /// </summary>
    public class GameObjectConditions
    {
		/// <summary>
		/// Checks if <paramref name="gameObject"/> is active.
		/// </summary>
		/// <remarks>
		/// This is equivalent to [{gameObject}.activeSelf](https://docs.unity3d.com/ScriptReference/GameObject-activeSelf.html), not [{gameObject}.activeInHierarchy](https://docs.unity3d.com/ScriptReference/GameObject-activeInHierarchy.html).
		/// </remarks>
		public bool Active(GameObject gameObject) => default;

		/// <summary>
		/// Checks if the distance between the current edit object and Vector3(<paramref name="x"/>, <paramref name="y"/>, <paramref name="z"/>) is <paramref name="distance"/> or less.
		/// </summary>
		/// <remarks>
		/// This is equivalent to [EditObjectNearPoint:{x}, {y}, {z}:{distance}](xref:kg_PoseMod.GameObjectConditions#kg_PoseMod_GameObjectConditions_EditObjectNearPoint_Vector3_System_Single_).
		/// </remarks>
		public bool EditObjectNearPoint(float x, float y, float z, float distance) => default;

		/// <summary>
		/// Checks if the distance between the current edit object and <paramref name="location"/> is <paramref name="distance"/> or less.
		/// </summary>
		/// <remarks>
		/// This is equivalent to [ObjectNearPoint:(#EDIT_OBJECT):{location}:{distance}](xref:kg_PoseMod.GameObjectConditions#kg_PoseMod_GameObjectConditions_ObjectNearPoint_GameObject_Vector3_System_Single_).
		/// </remarks>
		public bool EditObjectNearPoint(Vector3 location, float distance) => default;

		/// <summary>
		/// Checks if the distance between the current edit object and <paramref name="gameObject"/> is <paramref name="distance"/> or less.
		/// </summary>
		/// <remarks>
		/// This is equivalent to [ObjectNearOtherObject:(#EDIT_OBJECT):{gameObject}:{distance}](xref:kg_PoseMod.GameObjectConditions#kg_PoseMod_GameObjectConditions_ObjectNearOtherObject_GameObject_GameObject_System_Single_).
		/// </remarks>
		public bool EditObjectNearOtherObject(GameObject gameObject, float distance) => default;

		/// <summary>
		/// Checks if <paramref name="gameObject"/> exists.
		/// </summary>
		public bool Exists(GameObject gameObject) => default;

		/// <summary>
		/// Checks if the distance between <paramref name="gameObject"/> and <paramref name="location"/> is <paramref name="distance"/> or less.
		/// </summary>
		public bool ObjectNearPoint(GameObject gameObject, Vector3 location, float distance) => default;

		/// <summary>
		/// Checks if the distance between <paramref name="gameObject1"/> and <paramref name="gameObject2"/> is <paramref name="distance"/> or less.
		/// </summary>
		public bool ObjectNearOtherObject(GameObject gameObject1, GameObject gameObject2, float distance) => default;
	}
}
