﻿namespace kg_PoseMod
{
    /// <summary>
    /// Contains number-related Pose Mod conditions that work on any application.
    /// </summary>
    public class NumberConditions
    {
        /// <summary>
        /// Checks if the numbers <paramref name="value1"/> and <paramref name="value2"/> are equal.
        /// </summary>
        public bool Equal(float value1, float value2) => default;

        /// <summary>
        /// Checks if the numbers <paramref name="value1"/> and <paramref name="value2"/> are different.
        /// </summary>
        public bool NotEqual(float value1, float value2) => default;

        /// <summary>
        /// Checks if <paramref name="value1"/> is larger than <paramref name="value2"/>.
        /// </summary>
        public bool Larger(float value1, float value2) => default;
    }
}
