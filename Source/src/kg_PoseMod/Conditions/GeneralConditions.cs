﻿namespace kg_PoseMod
{
	/// <summary>
	/// Contains Pose Mod conditions that work on any application.
	/// </summary>
	public class GeneralConditions
	{
		/// <summary>
		/// A test condition that simply returns <paramref name="value"/>.
		/// </summary>
		public bool _test(bool value) => default;

		/// <summary>
		/// Checks if the key with the name "<paramref name="value"/>" is currently pressed.
		/// </summary>
		/// <remarks>
		/// This is equivalent to [Input.GetKey({value})](https://docs.unity3d.com/ScriptReference/Input.GetKey.html).
		/// </remarks>
		public bool Key(string value) => default;

		/// <summary>
		/// Checks if <paramref name="value"/> is true.
		/// </summary>
		public bool IsTrue(bool value) => default;
	}
}
