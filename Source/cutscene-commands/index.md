# Cutscene Commands

This section contains documentation on the various cutscene commands and conditions (just commands, for short) available in the Pose Mod.

Due to the way that DocFX works, the documentation uses methods to represent commands. A method declaration looks something like this:
```csharp
public bool ObjectNearPoint(GameObject gameObject, Vector3 location, float distance)
```
For commands, the first keyword is `void`, meaning that they return nothing. For conditions, it is `bool`. Some commands do not have parameters.

The above declaration would translate to the following Pose Mod condition, for example:
`if:ObjectNearPoint:Player:0, 4, -20:5`

Additionally, if a method name contains `__`...
```csharp
public void Sun__Color(Color color)
```
...it shall be interpreted as `.`:
```
// Sets the sun's color to white:
Sun.Color:1, 1, 1, 1
```
