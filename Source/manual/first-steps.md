# First Steps

## Installing

To install the Pose Mod, copy the following files and folder to the application's root folder:
- `kg_PoseMod.exe` (required) — The Pose Mod's hook.
- `kgcorlib.dll` (required) — The Pose Mod itself, plus a small amount of cutscene commands that work on any Unity application.
- `kg_PoseMod.dll` (optional; strongly recommended) — Cutscene commands and conditions that work on any Unity application.
- `YandereSimulator_Data` (optional) — Examples, many of which require `pm_yan.dll`.
- `pm_yan.dll` (optional; strongly recommended for Yandere Simulator) — Cutscene commands and conditions that work only on Yandere Simulator.

The Pose Mod is compatible with any Unity 4, 5, or 2017 application that uses the Mono scripting backend.

### The Structure of a Unity Application

The root folder of a Unity application contains a `Game.exe` file, a `Game_Data` folder, and some other files depending on the Unity version and scripting backend.

### Checking the Unity Version

To check the Unity version of a Unity application, hover your mouse over `Game.exe` and look at the `File version` field, something like `2017.2.3.47849`. You only care about the first number.

### Checking the Scripting Backend

If the application's data folder contains a `Managed` folder, it uses Mono.

If the application's root folder contains a file called `GameAssembly.dll`, it uses IL2CPP and is unsupported by the Pose Mod.

## Running

There are three ways to run the Pose Mod.

### Method 1

Run `kg_PoseMod.exe`.

This method only works with Yandere Simulator.

### Method 2

Drag the application's executable into `kg_PoseMod.exe`.

### Method 3

1. Create a file called `pmprocid.txt` in the application's root folder.
2. Run the application and the Pose Mod (`kg_PoseMod.exe`).
3. Open the Task Manager (Ctrl+Shift+Esc) and go to the `Details` tab.
4. Find the application in the list and get its PID (Process ID). A process' ID is different every time it runs.
5. Type the application's PID in the Pose Mod's window and press enter.

This method allows you to run the Pose Mod while the application is running.
