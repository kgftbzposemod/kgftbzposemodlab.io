# Credits

Made by [kgftbz].

Thanks to [Johny Michelson] for help with the sound script.

Cutscene Commands:
- [3l H4ck3r C0mf0r7]
- [AMZE | Ramon]
- [kgftbz]
- KuuderessioPlusvalín

Example Cutscenes:
- [kgftbz]
- [Ramobo]
- [Tyolyl Schmobop]

Bundle Files:
- Yanderesetto

The documentation was made by [Ramobo] using [DocFX] and is hosted by [GitLab Pages].

[3l H4ck3r C0mf0r7]: https://www.youtube.com/channel/UCXum6kA0GHY3OGJyPaUOmoA
[AMZE | Ramon]: https://www.youtube.com/channel/UCqu4couQ7sNU7fnC67YYovQ
[DocFX]: https://dotnet.github.io/docfx/index.html
[GitLab Pages]: https://about.gitlab.com/stages-devops-lifecycle/pages/
[Johny Michelson]: https://www.youtube.com/channel/UC3A4h5Ydl3bid2xnYdsyF1Q
[kgftbz]: https://www.youtube.com/channel/UCt4GAyZM70kk9-brR0WLRMw
[Ramobo]: https://ramobo.gitlab.io
[Tyolyl Schmobop]: https://www.youtube.com/channel/UCAGik89vfZ86fPl963OBHew
