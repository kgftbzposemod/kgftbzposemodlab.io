# Pose Mod

The Pose Mod is a mod created by **kgftbz** for posing characters in and modding Unity games, originally for Yandere Simulator only. You can download the Pose Mod on [MEGA](http://ceesty.com/w45aXY).

Above, the [Manual](manual/first-steps.md) section contains information on using the Pose Mod. The [Cutscene Commands](cutscene-commands/index.md) section contains description of the various cutscene commands available in the Pose Mod. **The documentation is unfinished.**

If you are on the offline documentation, which is included in the Pose Mod download, you can find the online version [here](https://kgftbzposemod.gitlab.io).
